/**
 * @author Ariawhan Putra
 */

const express = require("express");
const bodyParser = require("body-parser");
const path = require("path");
const routes = require("./../routes");
const publicDir = path.join(__dirname, "../public");
const viewsDir = path.join(__dirname, "./views");

const app = express();

// Set JSON request parser
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

// set view engine
app.set("views", viewsDir);
app.set("view engine", "hbs");

// set public static
app.use(express.static(publicDir));

// set routes
app.use(routes);

//The 404 Route (ALWAYS Keep this as the last route)
app.get("*", function (req, res) {
  res.status(404).send("<h1>404</h1>");
});

module.exports = {
  app,
};
