/**
 * @author Ariawhan putra
 */

const users = require("./usersRepositorie");
const product = require("./productRepositorie");

module.exports = {
  users,
  product,
};
