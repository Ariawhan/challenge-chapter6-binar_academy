/**
 * @author Ariawan Putra
 */

const { cars } = require("../../app/models");

// create products
const create = (body) => {
  return cars.create(body);
};

// update products
const update = (body, id) => {
  return cars.update(body, {
    where: {
      id: id,
    },
  });
};

// findAll products
const findAll = (attributes, where) => {
  return cars.findAll({
    attributes: attributes,
    where: where,
  });
};

// findByPk products
const findByPk = (attributes, id) => {
  return cars.findOne({
    attributes: attributes,
    where: {
      id: id,
    },
  });
};

module.exports = { create, update, findAll, findByPk };
