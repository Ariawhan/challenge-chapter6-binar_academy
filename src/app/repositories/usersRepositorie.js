/**
 * @author Ariawan Putra
 */

const { users } = require("../../app/models");

// create users
const create = (body) => {
  return users.create(body);
};

// update by id users
const update = (body, id) => {
  return users.update(body, {
    where: {
      id: id,
    },
  });
};

// findAll users
const findAll = (attributes) => {
  return users.findAll({
    attributes: attributes,
  });
};

// findOne by id users
const findOne = (attributes, where) => {
  return users.findOne({
    attributes: attributes,
    where: where,
  });
};

// findOne by id users
const findByPk = (attributes, id) => {
  return users.findByPk({
    attributes: attributes,
    where: {
      id: id,
    },
  });
};

module.exports = { create, update, findAll, findOne, findByPk };
