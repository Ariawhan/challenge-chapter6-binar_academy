/**
 * @author Ariawhan putra
 */

const servicesV1 = require("./v1");

module.exports = {
  servicesV1,
};
