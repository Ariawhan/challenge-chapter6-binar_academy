/**
 * @author Ariawan Putra
 */

const { libBcrypt, libJwt } = require("../../lib");
const repositories = require("../../repositories");

//create Users
const createUser = async (
  firstName,
  lastName,
  email,
  password,
  confirmPassword
) => {
  if (password != confirmPassword) {
    throw new Error("Password not match!");
  } else {
    password = await libBcrypt.hashing(password);
    email = email.toLowerCase();
    return repositories.users.create({
      firstName,
      lastName,
      email,
      password,
    });
  }
};

//Login Users
const loginService = async (email, password, rememberMe) => {
  email = email.toLowerCase();
  const user = await repositories.users.findOne(
    ["id", "firstName", "lastName", "email", "password"],
    {
      email: email,
    }
  );
  if (user == null) {
    throw new Error("User not found!");
  } else if ((await libBcrypt.compare(password, user.password)) == false) {
    throw new Error("Password not correct!");
  } else if ((await libBcrypt.compare(password, user.password)) == true) {
    const createToken = await libJwt.signToken(user, rememberMe);
    await repositories.users.update(
      {
        token: "Bearer " + createToken,
      },
      user.id
    );
    return {
      id: user.id,
      user: user.firstName + " " + user.lastName,
      token: createToken,
    };
  }
};

module.exports = {
  createUser,
  loginService,
};
