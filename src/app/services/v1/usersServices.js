/**
 * @author Ariawan Putra
 */

const repositories = require("../../repositories");
const { libBcrypt, libJwt } = require("../../lib");

// Showing users
const readUsers = async () => {
  return await repositories.users.findAll([
    "id",
    "firstName",
    "lastName",
    "email",
    "role",
    "createdAt",
  ]);
};

// Showing users by Id
const readUsersbyId = async (userId) => {
  console.log(userId);
  const user = await repositories.users.findOne(
    ["id", "firstName", "lastName", "email", "role", "createdAt"],
    { id: userId }
  );
  if (user != null) {
    return user;
  } else {
    throw new Error("User not found");
  }
};

// Update name and Email
const updateNameEmailService = async (firstName, lastName, email, idUser) => {
  email = email.toLowerCase();
  return await repositories.users.update(
    {
      firstName: firstName,
      lastName: lastName,
      email: email,
    },
    idUser
  );
};

//update password
const updatePasswordService = async (
  dataUser,
  password,
  confirmPassword,
  idUser
) => {
  const createToken = await libJwt.signToken(dataUser, true);
  if (password != confirmPassword) {
    throw new Error("Password not match");
  } else if (password == confirmPassword) {
    passwordHash = await libBcrypt.hashing(password);
    await repositories.users.update(
      {
        password: passwordHash,
        token: "Bearer " + createToken,
      },
      idUser
    );
  }
  return createToken;
};

// Update Role
const updateRoleService = async (dataUser, role, idUser) => {
  let roleName = "";
  const createToken = await libJwt.signToken(dataUser, true);
  if (role == 1) {
    roleName = "Admin";
  } else if (role == 2) {
    roleName = "Super Admin";
  } else if (role == 0) {
    roleName = "Member";
  } else {
    throw new Error("Role not found");
  }
  await repositories.users.update(
    {
      role: role,
      token: "Bearer" + createToken,
    },
    idUser
  );
  return { role: roleName, token: createToken };
};

module.exports = {
  readUsers,
  readUsersbyId,
  updateNameEmailService,
  updatePasswordService,
  updateRoleService,
};
