/**
 * @author Ariawan Putra
 */

const repositories = require("../../repositories");
const { Op } = require("sequelize");

//readProducts
const readProducts = async (method, idUser) => {
  if (method == "all") {
    return await repositories.product.findAll(
      [
        "id",
        "status",
        "plate",
        "manufacture",
        "model",
        "images",
        "rentPerDay",
        "capacity",
        "description",
        "availableAt",
        "transmission",
        "available",
        "type",
        "year",
        "options",
        "specs",
      ],
      { status: true }
    );
  } else if (method == "admin") {
    return await repositories.product.findAll(
      [
        "id",
        "status",
        "plate",
        "manufacture",
        "model",
        "images",
        "rentPerDay",
        "capacity",
        "description",
        "availableAt",
        "transmission",
        "available",
        "type",
        "year",
        "options",
        "specs",
      ],
      { status: true, createdBy: idUser }
    );
  } else if (method == "superAdmin") {
    return await repositories.product.findAll(
      [
        "id",
        "status",
        "plate",
        "manufacture",
        "model",
        "images",
        "rentPerDay",
        "capacity",
        "description",
        "availableAt",
        "transmission",
        "available",
        "type",
        "year",
        "options",
        "specs",
        "deleteBy",
        "createdBy",
        "updatedBy",
      ],
      {
        [Op.or]: [{ status: true }, { status: false }],
      }
    );
  }
};

// readProducts by Id
const readProductsById = async (productsId) => {
  const products = await repositories.product.findByPk(
    [
      "id",
      "status",
      "plate",
      "manufacture",
      "model",
      "images",
      "rentPerDay",
      "capacity",
      "description",
      "availableAt",
      "transmission",
      "available",
      "type",
      "year",
      "options",
      "specs",
      "deleteBy",
      "deleteAt",
      "updatedBy",
      "updatedAt",
      "createdAt",
    ],
    productsId
  );
  if (products == null) {
    throw new Error("Product not found");
  } else {
    return products;
  }
};

// delete products by id
const deleteProductService = async (productsId, userId) => {
  const products = await repositories.product.update(
    {
      status: false,
      deleteBy: userId,
      deleteAt: new Date(),
    },
    productsId
  );
  if (parseInt(products) == 0) {
    throw new Error("Product not found");
  } else {
    return products;
  }
};

// create products
const createProdutService = async (body) => {
  return await repositories.product.create(body);
};

// update products by idProduct
const updateProdutService = async (body, idProduct) => {
  return await repositories.product.update(body, idProduct);
};

module.exports = {
  readProducts,
  readProductsById,
  deleteProductService,
  createProdutService,
  updateProdutService,
};
