/**
 * @author Ariawhan putra
 */

const authServices = require("./authServices");
const productsServices = require("./productsServices");
const usersService = require("./usersServices");

module.exports = {
  authServices,
  productsServices,
  usersService,
};
