/**
 * @author Ariawhan putra
 */

const bcrypt = require("bcryptjs");

// comparing password with bcryptjs
const compare = async (password, encrypted) => {
  try {
    return await bcrypt.compare(password, encrypted);
  } catch (err) {
    throw new Error("Bcryptjs error comparing!");
  }
};

module.exports = {
  compare,
};
