/**
 * @author Ariawhan putra
 */

const { hashing } = require("./hashing");
const { compare } = require("./compare");

module.exports = {
  hashing,
  compare,
};
