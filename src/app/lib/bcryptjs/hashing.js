/**
 * @author Ariawhan putra
 */

const bcrypt = require("bcryptjs");

// salt for hashing default 10
const salt = parseInt(process.env.PASSWORD_SALT) || 10;

// hashing password with bcrypt
const hashing = async (password) => {
  try {
    const result = await bcrypt.hash(password, salt);
    return result;
  } catch (err) {
    throw new Error("Bcryptjs error hashing!");
  }
};

module.exports = {
  hashing,
};
