/**
 * @author Ariawhan putra
 */

const jwt = require("jsonwebtoken");

// private key with default "Fortune Cookie"
const privateKey = process.env.JWT_RIVATE_KEY || "Fortune Cookie";

// verify token with JWT
const verifyToken = async (token) => {
  try {
    return await jwt.verify(token.split("Bearer ")[1], privateKey);
  } catch (err) {
    return false;
  }
};

module.exports = { verifyToken };
