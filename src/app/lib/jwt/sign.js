/**
 * @author Ariawhan putra
 */

const jwt = require("jsonwebtoken");

// private key with default "Fortune Cookie"
const privateKey = process.env.JWT_RIVATE_KEY || "Fortune Cookie";

// private keycreate a new token with jwt
const signToken = async (data, exp) => {
  // create a new token with expired
  if (exp == true) {
    try {
      return await jwt.sign(
        {
          id: data.id,
          role: data.role,
          email: data.email,
          date: new Date(),
        },
        privateKey,
        {
          //expiresIn: "10h" // it will be expired after 10 hours
          expiresIn: "1d", // it will be expired after 1 days
          //expiresIn: 120 // it will be expired after 120ms
          //expiresIn: "20s", // it will be expired after 120s
        },
        // algorithm token
        { algorithm: "RS256" }
      );
    } catch (err) {
      throw new Error("JWT error sign token by expired!");
    }
    // create a new token without expired
  } else if (exp == false) {
    try {
      const token = await jwt.sign(
        {
          id: data.id,
          role: data.role,
          email: data.email,
          //   date: new Date(),
        },
        privateKey
      );
      return token;
    } catch (err) {
      throw new Error("JWT error sign token without expired!");
    }
  }
};

module.exports = {
  signToken,
};
