/**
 * @author Ariawhan putra
 */

const { signToken } = require("./sign");
const { verifyToken } = require("./verify");

module.exports = {
  signToken,
  verifyToken,
};
