/**
 * @author Ariawhan putra
 */

const api = require("./api");

module.exports = {
  api,
};
