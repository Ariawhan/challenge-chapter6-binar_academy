/**
 * @author Ariawhan Putra
 */

const { servicesV1 } = require("../../../services");

//Register Users
const registerUsers = async (req, res) => {
  const { firstName, lastName, email, password, confirmPassword } = req.body;
  try {
    const user = await servicesV1.authServices.createUser(
      firstName,
      lastName,
      email,
      password,
      confirmPassword
    );
    res.status(201).json({
      status: true,
      message: "User created successfully",
      data: {
        id: user.id,
        name: user.firstName + " " + user.lastName,
        email: user.email,
      },
    });
  } catch (err) {
    res.status(400).json({
      status: false,
      message: err.message,
    });
  }
};

// Login Users
const loginUsers = async (req, res) => {
  const { email, password, rememberMe } = req.body;
  try {
    const users = await servicesV1.authServices.loginService(
      email,
      password,
      rememberMe
    );
    res.status(201).json({
      status: true,
      data: users,
    });
  } catch (err) {
    res.status(400).json({
      status: false,
      message: err.message,
    });
  }
};

module.exports = {
  registerUsers,
  loginUsers,
};
