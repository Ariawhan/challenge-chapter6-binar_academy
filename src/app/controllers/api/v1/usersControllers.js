/**
 * @author Ariawhan Putra
 */

const { servicesV1 } = require("../../../services");

// Showing Users
const showUsers = async (req, res) => {
  try {
    users = await servicesV1.usersService.readUsers();
    res.status(200).json({
      status: true,
      data: users,
    });
  } catch (err) {
    res.status(400).json({
      message: err.message,
    });
  }
};

// Showing Users by Id
const showUsersbyId = (method) => {
  return async (req, res, next) => {
    const userId = req.params.id;
    try {
      const users = await servicesV1.usersService.readUsersbyId(userId);
      //   console.log(users);
      if (method == "forGet") {
        res.status(200).json({
          status: true,
          data: users,
        });
      } else if (method == "forUpdate") {
        req.user = users;
        next();
      } else {
        throw new Error("bad request");
      }
    } catch (err) {
      res.status(400).json({
        status: false,
        message: err.message,
      });
    }
  };
};

// Update name and Email of user
const updateNameEmail = async (req, res) => {
  const { firstName, lastName, email } = req.body;
  const idUser = req.params.id;
  try {
    await servicesV1.usersService.updateNameEmailService(
      firstName,
      lastName,
      email,
      idUser
    );
    res.status(201).json({
      status: true,
      message: "Update data id " + idUser + " successfully",
      data: {
        firstName: firstName,
        lastName: lastName,
        email: email,
      },
    });
  } catch (err) {
    res.status(400).json({
      status: false,
      message: err.message,
    });
  }
};

//update password users
const updatePassword = async (req, res) => {
  const { password, confirmPassword } = req.body;
  const idUser = req.params.id;
  try {
    const user = await servicesV1.usersService.updatePasswordService(
      req.user,
      password,
      confirmPassword,
      idUser
    );
    // console.log(req.user);
    res.status(201).json({
      status: true,
      message: "Update password id " + idUser + " successfully",
      data: {
        id: idUser,
        name: req.user.firstName + " " + req.user.lastName,
        newToken: user,
      },
    });
  } catch (err) {
    res.status(400).json({
      status: false,
      message: err.message,
    });
  }
};

// update role
const updateRole = async (req, res) => {
  const { role } = req.body;
  const idUser = req.params.id;
  try {
    const result = await servicesV1.usersService.updateRoleService(
      req.user,
      role,
      idUser
    );
    res.status(201).json({
      status: true,
      message:
        "Update role to " + result.role + " id " + idUser + " successfully",
      data: {
        role: result.role,
        newToken: result.token,
      },
    });
  } catch (err) {
    res.status(400).json({
      status: false,
      message: err.message,
    });
  }
};

module.exports = {
  showUsers,
  showUsersbyId,
  updateNameEmail,
  updatePassword,
  updateRole,
};
