/**
 * @author Ariawan Putra
 */

const auth = require("./authControllers");
const users = require("./usersControllers");
const products = require("./productsControllers");

module.exports = {
  auth,
  products,
  users,
};
