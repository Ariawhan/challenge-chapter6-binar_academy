/**
 * @author Ariawhan Putra
 */

const { servicesV1 } = require("../../../services");

// Showing all products
const swhoProducts = (method) => {
  return async (req, res) => {
    let idUser = "";
    if (method != "all") {
      idUser = req.user.id;
    }
    try {
      const products = await servicesV1.productsServices.readProducts(
        method,
        idUser
      );
      res.status(200).json({
        status: true,
        message: "Successfully get data product",
        data: products,
      });
    } catch (err) {
      res.status(400).json({
        status: false,
        message: err.message,
      });
    }
  };
};

// showProductById
const swhoProductsById = (method) => {
  return async (req, res, next) => {
    try {
      const products = await servicesV1.productsServices.readProductsById(
        req.params.id
      );
      if (method === "forGet") {
        res.status(200).json({
          status: true,
          message: "Successfully get data product id : " + req.params.id,
          data: products,
        });
      } else if (method === "forUpdate") {
        next();
      } else {
        throw new Error("bad request");
      }
    } catch (err) {
      res.status(400).json({
        status: false,
        message: err.message,
      });
    }
  };
};

const deleteProduct = async (req, res) => {
  // console.log(req.user);
  try {
    const products = await servicesV1.productsServices.deleteProductService(
      req.params.id,
      req.user.id
    );
    res.status(200).json({
      status: true,
      message:
        "Successfully delete product car id : " +
        req.params.id +
        " by : " +
        req.user.name,
    });
  } catch (err) {
    res.status(400).json({
      status: false,
      message: err.message,
    });
  }
};

const createProdut = async (req, res) => {
  // console.log(req.user.id);
  const body = {
    plate: req.body.plate,
    manufacture: req.body.manufacture,
    model: req.body.model,
    images: "./uploads/" + req.file.filename,
    rentPerDay: req.body.rentPerDay,
    capacity: req.body.capacity,
    description: req.body.description,
    availableAt: req.body.availableAt,
    transmission: req.body.transmission,
    available: req.body.available,
    type: req.body.type,
    year: req.body.year,
    options: req.body.options.split(","),
    specs: req.body.specs.split(","),
    createdBy: req.user.id,
  };
  try {
    const product = await servicesV1.productsServices.createProdutService(body);
    res.status(201).json({
      status: true,
      message: "Successfully create product car",
      data: product,
    });
  } catch (err) {
    res.status(400).json({
      status: false,
      message: err.message,
    });
  }
};

const updateProdut = async (req, res) => {
  // console.log(req.user.id);
  const idProduct = req.params.id;
  const body = {
    plate: req.body.plate,
    manufacture: req.body.manufacture,
    model: req.body.model,
    images: "./uploads/" + req.file.filename,
    rentPerDay: req.body.rentPerDay,
    capacity: req.body.capacity,
    description: req.body.description,
    availableAt: req.body.availableAt,
    transmission: req.body.transmission,
    available: req.body.available,
    type: req.body.type,
    year: req.body.year,
    options: req.body.options.split(","),
    specs: req.body.specs.split(","),
    updatedBy: req.user.id,
  };
  try {
    const product = await servicesV1.productsServices.updateProdutService(
      body,
      idProduct
    );
    res.status(200).json({
      status: true,
      message: "Successfully update product car id " + idProduct,
      data: body,
    });
  } catch (err) {
    res.status(400).json({
      status: false,
      message: err.message,
    });
  }
};

module.exports = {
  swhoProducts,
  swhoProductsById,
  deleteProduct,
  createProdut,
  updateProdut,
};
