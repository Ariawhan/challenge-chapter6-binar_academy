/**
 * @author Ariawhan putra
 */

const multer = require("multer");
const fs = require("fs");

const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    // directory
    const dirPath = "./src/public/uploads";
    // check if directory exists
    if (fs.existsSync(dirPath)) {
      cb(null, dirPath);
      // console.log("Directory exists!");
    } else {
      fs.mkdirSync(dirPath);
      cb(null, dirPath);
      // console.log("Directory not found.");
    }
  },
  filename: function (req, file, cb) {
    // create filename
    cb(null, new Date().toISOString().replace(/:/g, "-") + ".jpg");
  },
});

// multer uploads
const uploads = multer({ storage });

module.exports = uploads;
