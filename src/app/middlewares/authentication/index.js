/**
 * @author Ariawhan putra
 */

const { users } = require("../../models");
const { libJwt } = require("../../lib");

const checkToken = async (req, res, next) => {
  try {
    console.log(req.headers.authorization);
    const checkTokenUser = await libJwt.verifyToken(req.headers.authorization);
    if (checkTokenUser != false) {
      const tokenDatabase = await users.findByPk(checkTokenUser.id);
      if (tokenDatabase != null) {
        if (tokenDatabase.token == req.headers.authorization) {
          req.user = {
            id: tokenDatabase.id,
            name: tokenDatabase.firstName + " " + tokenDatabase.lastName,
            role: tokenDatabase.role,
          };
          // console.log(req.user);
          next();
        } else {
          throw new Error("Token expired by login");
        }
      } else {
        throw new Error("Invalid Users");
      }
    } else if (checkTokenUser == false) {
      throw new Error("Invalid Token");
    }
  } catch (err) {
    res.status(401).json({
      status: false,
      message: err.message,
    });
  }
};
module.exports = {
  checkToken,
};
