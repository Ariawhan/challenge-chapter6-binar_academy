/**
 * @author Ariawhan putra
 */

const checkIdentity = (number, method) => {
  if (method == "role") {
    return async (req, res, next) => {
      try {
        //check of array role
        let found = false;
        for (let i = 0; i < number.length; i++) {
          if (req.user.role == number[i]) {
            // console.log("Masuk " + number[i]);
            // next();
            found = true;
          }
        }
        // console.log(found);
        if (found == true) {
          next();
        } else if (found == false) {
          throw new Error("You don't have approval to do this!");
        } else {
          throw new Error("Bad Request");
        }
      } catch (err) {
        res.status(400).json({
          status: false,
          message: err.message,
        });
      }
    };
  } else if (method == "id") {
    return async (req, res, next) => {
      if (req.user.id == req.params.id) {
        // console.log("boleh");
        next();
      } else if (req.user.role == number) {
        // console.log("boleh juga");
        next();
      } else {
        res.status(400).json({
          status: false,
          message: "You don't have approval to do this!",
        });
      }
    };
  } else {
    return async (req, res) => {
      res.status(400).json({
        status: false,
        message: "Bad Request",
      });
    };
  }
};

module.exports = { checkIdentity };
