/**
 * @author Ariawhan putra
 */

const authentication = require("./authentication");
const identiry = require("./identity");
const uploads = require("./uploads");

module.exports = {
  authentication,
  identiry,
  uploads,
};
