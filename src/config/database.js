/**
 * @author Ariawhan Putra
 */

// Database Configuration
const {
  DB_USERNAME = null,
  DB_PASSWORD = null,
  DB_HOST = "127.0.0.1",
  DB_NAME = "database",
  DB_DIALECT = "postgres",
} = process.env;

module.exports = {
  development: {
    username: DB_USERNAME,
    password: DB_PASSWORD,
    database: DB_NAME,
    host: DB_HOST,
    dialect: DB_DIALECT,
  },
};
