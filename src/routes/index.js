/**
 * @author Ariawhan Putra
 */

const router = require("express").Router();
const apiRouter = require("./api");
const swaggerUi = require("swagger-ui-express");
const swaggerJsdoc = require("swagger-jsdoc");
const swaggerDoc = require("../../docs/api");
const cors = require("cors");

const specs = swaggerJsdoc(swaggerDoc.options);
router.use(cors());

router.get("/", (req, res) => {
  res.render("index.hbs");
});

// API documentation
router.use("/api-docs", swaggerUi.serve, swaggerUi.setup(specs));

//Api Router
router.use("/api", apiRouter);

module.exports = router;
