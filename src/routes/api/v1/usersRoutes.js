/**
 * @author Ariawhan Putra
 */

const router = require("express").Router();
const controllers = require("../../../app/controllers");
const middlewares = require("../../../app/middlewares");

// Get all users just super admin
router.get(
  "/",
  middlewares.authentication.checkToken,
  middlewares.identiry.checkIdentity([2], "role"),
  controllers.api.apiV1.users.showUsers
);
// Get user by id just user with the id and super admin
router.get(
  "/:id",
  middlewares.authentication.checkToken,
  middlewares.identiry.checkIdentity([2], "id"),
  controllers.api.apiV1.users.showUsersbyId("forGet")
);

// update users name and email just user with the id and super admin
router.put(
  "/:id",
  middlewares.authentication.checkToken,
  middlewares.identiry.checkIdentity([2], "id"),
  controllers.api.apiV1.users.showUsersbyId("forUpdate"),
  controllers.api.apiV1.users.updateNameEmail
);

// update password just user with the id and super admin
router.put(
  "/password/:id",
  middlewares.authentication.checkToken,
  middlewares.identiry.checkIdentity([2], "id"),
  controllers.api.apiV1.users.showUsersbyId("forUpdate"),
  controllers.api.apiV1.users.updatePassword
);

// update role just super admin
router.put(
  "/role/:id",
  middlewares.authentication.checkToken,
  middlewares.identiry.checkIdentity([2], "role"),
  controllers.api.apiV1.users.showUsersbyId("forUpdate"),
  controllers.api.apiV1.users.updateRole
);

module.exports = router;
