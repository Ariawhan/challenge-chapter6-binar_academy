/**
 * @author Ariawhan Putra
 */

const router = require("express").Router();
const controllers = require("../../../app/controllers");
const middlewares = require("../../../app/middlewares");

// register and login users
router.post("/register", controllers.api.apiV1.auth.registerUsers);
router.post("/login", controllers.api.apiV1.auth.loginUsers);

module.exports = router;
