/**
 * @author Ariawhan Putra
 */
const router = require("express").Router();
const apiProduct = require("./productsRoutes");
const apiAuth = require("./authRoutes");
const apiUser = require("./usersRoutes");

// Setup Routes for API
router.use("/products", apiProduct);
router.use("/auth", apiAuth);
router.use("/users", apiUser);

module.exports = router;
