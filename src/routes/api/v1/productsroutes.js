/**
 * @author Ariawhan Putra
 */

const router = require("express").Router();
const controllers = require("../../../app/controllers");
const middlewares = require("../../../app/middlewares");

// get all data product Cars
router.get("/", controllers.api.apiV1.products.swhoProducts("all"));

// get all for admin by id admin id
router.get(
  "/admin",
  middlewares.authentication.checkToken,
  middlewares.identiry.checkIdentity([1], "role"),
  controllers.api.apiV1.products.swhoProducts("admin")
);

// get all data for super admin
router.get(
  "/superAdmin",
  middlewares.authentication.checkToken,
  middlewares.identiry.checkIdentity([2], "role"),
  controllers.api.apiV1.products.swhoProducts("superAdmin")
);

// Get data product Cars By Id
router.get(
  "/:id",
  middlewares.authentication.checkToken,
  middlewares.identiry.checkIdentity([1, 2], "role"),
  controllers.api.apiV1.products.swhoProductsById("forGet")
);

// Delete product data product with id just admin and super admin
router.delete(
  "/:id",
  middlewares.authentication.checkToken,
  middlewares.identiry.checkIdentity([1, 2], "role"),
  controllers.api.apiV1.products.deleteProduct
);

//Create data cars just admin and super admin
router.post(
  "/",
  middlewares.authentication.checkToken,
  middlewares.identiry.checkIdentity([1, 2], "role"),
  middlewares.uploads.single("images"),
  controllers.api.apiV1.products.createProdut
);

//Update data cars just uploader and super admin
router.put(
  "/:id",
  middlewares.authentication.checkToken,
  middlewares.identiry.checkIdentity([1, 2], "role"),
  middlewares.uploads.single("images"),
  controllers.api.apiV1.products.updateProdut
);
module.exports = router;
