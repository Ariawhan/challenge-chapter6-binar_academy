/**
 * @author Ariawhan Putra
 */
const router = require("express").Router();
const apiV1 = require("./v1");

// Setup Routes for API V1
router.use("/v1", apiV1);

module.exports = router;
