/**
 * @author Ariawhan Putra
 */

const port = process.env.SERVER_PORT || 8000;

const options = {
  definition: {
    openapi: "3.0.0",
    info: {
      title: "BCR - Binar Car Rental",
      version: "1.0.0",
      description:
        "This project challenge chapter 6 to learn Open API, Design Pattern, Async Process, Authentication & Authorization",
    },
    _servers: [
      {
        url: "http://localhost:" + port,
        description: "Development server",
      },
    ],
    paths: {
      "/api/v1/auth/register": {
        post: {
          tags: ["Auth Users"],
          summary: "Register New Users",
          consumes: "application/json",
          description: "Register new users. default : member",
          requestBody: {
            content: {
              "application/json": {
                schema: {
                  type: "object",
                  properties: {
                    firstName: { type: "string" },
                    lastName: { type: "string" },
                    email: { type: "string" },
                    password: { type: "string" },
                    confirmPassword: { type: "string" },
                  },
                },
                examples: {
                  ex_registers: {
                    description: "Data Register for users",
                    value: {
                      firstName: "Oppa",
                      lastName: "Ariawhan",
                      email: "mail@ariawhan.com",
                      password: "loveyoupacarorang",
                      confirmPassword: "loveyoupacarorang",
                    },
                  },
                },
              },
            },
          },
          responses: {
            201: {
              description: "User created successfully",
            },
            400: {
              description: "Bad Request!",
            },
          },
        },
      },
      "/api/v1/auth/login": {
        post: {
          tags: ["Auth Users"],
          summary: "Login Users",
          description:
            "Login Users and get JWT token. use rememberMe add expired. ex: false or true",
          consumes: "application/json",
          requestBody: {
            content: {
              "application/json": {
                schema: {
                  type: "object",
                  properties: {
                    email: { type: "string" },
                    password: { type: "string" },
                    rememberMe: { type: "boolean" },
                  },
                },
                examples: {
                  ex_SuperAdmin: {
                    description: "Data Login Super Admin with expired",
                    value: {
                      email: "superadmin@gmail.com",
                      password: "superadmin",
                      rememberMe: true,
                    },
                  },
                  ex_Admin: {
                    description: "Data Login Admin without expired",
                    value: {
                      email: "admin@gmail.com",
                      password: "admin",
                      rememberMe: false,
                    },
                  },
                  ex_member: {
                    description: "Data Login Super Admin without expired",
                    value: {
                      email: "user@gmail.com",
                      password: "user",
                      rememberMe: false,
                    },
                  },
                },
              },
            },
          },
          responses: {
            201: {
              description: "Login successfully",
            },
            400: {
              description: "Bad Request",
            },
          },
        },
      },
      "/api/v1/users/": {
        get: {
          security: [
            {
              CarAuth: [],
            },
          ],
          tags: ["Management Users"],
          summary: "Get all Users",
          description: "Get All Users Cars Rental",
          responses: {
            200: {
              description: "Sukses get all users",
            },
            400: {
              description: "Bad Request",
            },
          },
        },
      },
      "/api/v1/users/{id}": {
        get: {
          security: [
            {
              CarAuth: [],
            },
          ],
          tags: ["Management Users"],
          summary: "Get Users by id",
          description: "Get users by id",
          parameters: [
            {
              $ref: "#/components/parameters/id",
            },
          ],
          responses: {
            200: {
              description: "Sukses get users",
            },
            400: {
              description: "Bad Request",
            },
          },
        },
        put: {
          security: [
            {
              CarAuth: [],
            },
          ],
          tags: ["Management Users"],
          summary: "Update Users",
          description: "Update users name and email by id users",
          parameters: [
            {
              $ref: "#/components/parameters/id",
            },
          ],
          requestBody: {
            content: {
              "application/json": {
                schema: {
                  type: "object",
                  properties: {
                    firstName: { type: "string", example: "Ariawhan" },
                    lastName: { type: "string", example: "Putra" },
                    email: { type: "string", example: "newmail@ariawhan.com" },
                  },
                },
              },
            },
          },
          responses: {
            201: {
              description: "Update data id users successfully",
            },
            400: {
              description: "Bad Request",
            },
          },
        },
      },
      "/api/v1/users/role/{id}": {
        put: {
          security: [
            {
              CarAuth: [],
            },
          ],
          tags: ["Management Users"],
          summary: "Update role Users",
          description:
            "Update role users by id users to (super admin[2], admin[1], member[0])",
          parameters: [
            {
              $ref: "#/components/parameters/id",
            },
          ],
          requestBody: {
            content: {
              "application/json": {
                schema: {
                  type: "object",
                  properties: {
                    role: { type: "integer", example: "1" },
                  },
                },
              },
            },
          },
          responses: {
            201: {
              description: "Update role with id users successfully",
            },
            400: {
              description: "Bad Request",
            },
          },
        },
      },
      "/api/v1/users/password/{id}": {
        put: {
          security: [
            {
              CarAuth: [],
            },
          ],
          tags: ["Management Users"],
          summary: "Update password Users",
          description:
            "Update password users just users with id and super admin",
          parameters: [
            {
              $ref: "#/components/parameters/id",
            },
          ],
          requestBody: {
            content: {
              "application/json": {
                schema: {
                  type: "object",
                  properties: {
                    password: { type: "string", example: "loveyoupacarorang" },
                    confirmPassword: {
                      type: "string",
                      example: "loveyoupacarorang",
                    },
                  },
                },
              },
            },
          },
          responses: {
            201: {
              description: "Update password with id users successfully",
            },
            400: {
              description: "Bad Request",
            },
          },
        },
      },
      "/api/v1/products": {
        get: {
          tags: ["Product Cars"],
          summary: "Get data products (public)",
          description: "Get data products for public",
          responses: {
            200: {
              description: "Successfully get data product",
            },
            400: {
              description: "Bad Request",
            },
          },
        },
      },
      "/api/v1/products/admin/": {
        get: {
          security: [
            {
              CarAuth: [],
            },
          ],
          tags: ["Product Cars"],
          summary: "Get data products (admin)",
          description: "Get data products for admin",
          responses: {
            200: {
              description: "Successfully get data product",
            },
            400: {
              description: "Bad Request",
            },
          },
        },
      },
      "/api/v1/products/superAdmin/": {
        get: {
          security: [
            {
              CarAuth: [],
            },
          ],
          tags: ["Product Cars"],
          summary: "Get data products (Super Admin)",
          description: "Get data products for Super Admin",
          responses: {
            200: {
              description: "Successfully get data product",
            },
            400: {
              description: "Bad Request",
            },
          },
        },
      },
      "/api/v1/products/{id}": {
        get: {
          security: [
            {
              CarAuth: [],
            },
          ],
          tags: ["Product Cars"],
          summary: "Get data products by id (Product)",
          description: "Get data products by id (Product)",
          parameters: [
            {
              $ref: "#/components/parameters/id",
            },
          ],
          responses: {
            200: {
              description: "Successfully get data product",
            },
            400: {
              description: "Bad Request",
            },
          },
        },
      },
      "/api/v1/products/": {
        post: {
          security: [
            {
              CarAuth: [],
            },
          ],
          tags: ["Product Cars"],
          summary: "Create Product",
          description: "create a new product",
          consumes: "multipart/form-data",
          requestBody: {
            content: {
              "multipart/form-data": {
                schema: {
                  type: "object",
                  properties: {
                    plate: { type: "string", example: "plate" },
                    manufacture: { type: "string", example: "manufacture" },
                    model: { type: "string", example: "veloz" },
                    images: { type: "string", format: "binary" },
                    rentPerDay: { type: "integer", example: 300000 },
                    description: {
                      type: "string",
                      example:
                        "Membahas mengenai harga toyota veloz, perlu diketahui bahwa harga mobil ini dibedakan dalam beberapa tipe, setidaknya hingga saat rilis minggu lalu Toyota Veloz hadir dengan 3 tipe. Yaitu tipe Veloz 1.5 M/T, Veloz 1.5 Q CVT dan tipe tertingginya adalah Veloz 1.5 Q CVT TSS.",
                    },
                    availableAt: {
                      type: "date",
                      example: "2022-03-23T15:49:05.563Z",
                    },
                    transmission: { type: "string", example: "Automatic" },
                    available: { type: "boolean", example: true },
                    type: { type: "string", example: "SUV" },
                    year: { type: "integer", example: "2022" },
                    options: { type: "array[]", example: "Cruise Control" },
                    specs: { type: "array[]", example: "Brake assist" },
                  },
                },
              },
            },
          },
          responses: {
            201: {
              description: "Successfully get data product",
            },
            400: {
              description: "Bad Request",
            },
          },
        },
      },
      "/api/v1//products/{id}": {
        put: {
          security: [
            {
              CarAuth: [],
            },
          ],
          tags: ["Product Cars"],
          summary: "Update Product",
          description: "Update Product by id Product",
          consumes: "multipart/form-data",
          parameters: [
            {
              $ref: "#/components/parameters/id",
            },
          ],
          requestBody: {
            content: {
              "multipart/form-data": {
                schema: {
                  type: "object",
                  properties: {
                    plate: { type: "string", example: "plate" },
                    manufacture: { type: "string", example: "manufacture" },
                    model: { type: "string", example: "veloz" },
                    images: { type: "string", format: "binary" },
                    rentPerDay: { type: "integer", example: 300000 },
                    description: {
                      type: "string",
                      example:
                        "Membahas mengenai harga toyota veloz, perlu diketahui bahwa harga mobil ini dibedakan dalam beberapa tipe, setidaknya hingga saat rilis minggu lalu Toyota Veloz hadir dengan 3 tipe. Yaitu tipe Veloz 1.5 M/T, Veloz 1.5 Q CVT dan tipe tertingginya adalah Veloz 1.5 Q CVT TSS.",
                    },
                    availableAt: {
                      type: "date",
                      example: "2022-03-23T15:49:05.563Z",
                    },
                    transmission: { type: "string", example: "Automatic" },
                    available: { type: "boolean", example: true },
                    type: { type: "string", example: "SUV" },
                    year: { type: "integer", example: "2022" },
                    options: { type: "array[]", example: "Cruise Control" },
                    specs: { type: "array[]", example: "Brake assist" },
                  },
                },
              },
            },
          },
          responses: {
            201: {
              description: "Successfully get data product",
            },
            400: {
              description: "Bad Request",
            },
          },
        },
      },
      "/api/v1/products/{id}": {
        delete: {
          security: [
            {
              CarAuth: [],
            },
          ],
          tags: ["Product Cars"],
          summary: "delete data products by id (Product)",
          description: "delete data products by id (Product)",
          parameters: [
            {
              $ref: "#/components/parameters/id",
            },
          ],
          responses: {
            200: {
              description: "Successfully delete product cars",
            },
            400: {
              description: "Bad Request",
            },
          },
        },
      },
    },
    //for security JWT
    components: {
      securitySchemes: {
        CarAuth: {
          description: "Authentication for Car Management API",
          type: "http",
          scheme: "bearer",
          bearerFormat: "JWT",
        },
      },
      //for parameters
      parameters: {
        id: {
          name: "id",
          in: "path",
          required: true,
          schema: {
            type: "string",
            minLength: 1,
            maxLength: 100,
          },
        },
      },
    },
  },
  apis: ["../../src/routes"],
};

module.exports = {
  options,
};
