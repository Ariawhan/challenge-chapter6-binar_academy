# Daily task sequelize

This Team Project challenge chapter 6 from Binar Academy with mentor [Imam Hermawan](https://gitlab.com/ImamTaufiqHermawan)

#### Develop by

- [Ariawhan](https://gitlab.com/Ariawhan)

## Mockup sistem

Bellow is the views made with hbs view engine and bootstrap :

![Alt-Text](/docs/images/swagger.png)

![Alt-Text](/docs/images/landing.png)

![Alt-Text](/docs/images/cars.png)

## Entity Relationship Diagram

![Alt-Text](/docs/images/erd.png)

## Module node.js

```
- npm install
```

## Configuration Database

```
# Creat Database
$ npm run db:create

## Migrate database
$ npm run db:migrate

### Deed Database
$ npm run db:seed
```

## Run Server

> $ npm run dev
